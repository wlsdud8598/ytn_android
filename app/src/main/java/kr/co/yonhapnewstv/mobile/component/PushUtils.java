package kr.co.yonhapnewstv.mobile.component;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;


/**
 * 공통 메시지 다이얼로그
 */
public class PushUtils{
    private static final String TAG = PushUtils.class.getSimpleName();

    private static PowerManager.WakeLock mWakeLock;

    public static void acquireWakeLock(Context context){
        Log.i(TAG, "acquireWakeLock : "+mWakeLock);

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(
                PowerManager.FULL_WAKE_LOCK |
                        PowerManager.ACQUIRE_CAUSES_WAKEUP |
                        PowerManager.ON_AFTER_RELEASE, "WAKEUP"
        );
        mWakeLock.acquire();
        Log.i(TAG, "mWakeLock mWakeLock : "+mWakeLock);
    }

    public static void releaseWakeLock(){
        if(mWakeLock != null){
            mWakeLock.release();
            mWakeLock = null;
        }
        Log.i(TAG, "mWakeLock : "+mWakeLock);
    }

}