package kr.co.yonhapnewstv.mobile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import kr.co.yonhapnewstv.mobile.component.CircleIndicator;
import kr.co.yonhapnewstv.mobile.util.SharedPref;

public class GuideActivity extends BaseActivity implements View.OnClickListener {

    protected final String TAG = getClass().getSimpleName();
    public static GuideActivity activity = null;    //액티비티 변수 선언

    private String encodedToken;
    private String receivedUrl;

    public static final int ITEM_COUNT  = 5;

    private ViewPager mViewPager;
    private TextView textChange_1;
    private TextView textChange_2;
    private ImageView imgView;
    private RelativeLayout layoutTop;
    private LinearLayout layoutTop1;
    private LinearLayout layoutTop2;


    private CustomAdapter mViewPagerAdapter;
    private CircleIndicator mIndicator;

    private ImageButton mCloseBtn;

    private boolean Is_Setting = false;

    private int mCurrentPage = 0;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.guide_activity);
        activity = this;

        SharedPref.getInstance().initContext(this);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if(extras != null) {
            Is_Setting = extras.getBoolean("Is_Setting");
        }

        init();
        setEvent();
    }

    /**
     * 초기화
     */
    public void init(){

        mCloseBtn = findViewById(R.id.close_btn);
        mViewPager = findViewById(R.id.login_viewpager);
        mViewPagerAdapter = new CustomAdapter(GuideActivity.this);
        mIndicator = findViewById(R.id.indicator);

        mViewPager.setAdapter(mViewPagerAdapter);
//        mViewPager.setCurrentItem((ITEM_COUNT * 10000), false);
        mViewPagerAdapter.notifyDataSetChanged();
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);

        //원사이의 간격
        mIndicator.setItemMargin(15);
        //애니메이션 속도
        mIndicator.setAnimDuration(300);
        //indicator 생성
        mIndicator.createDotPanel(ITEM_COUNT, R.drawable.indicator_disable , R.drawable.indicator_able);
    }

    /**
     * 이벤트 연결
     */
    public void setEvent(){

        mCloseBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.close_btn:    // 서비스 시작하기
                SharedPref.getInstance().savePreferences(SharedPref.NEVER_OPEN, "yes");
                goMain();
                finish();
                break;

        }
    }

    /**
     * ViewPager 전환시 호출되는 메서드
     */
    private ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            mIndicator.selectDot(position);
            mCurrentPage = position;
            Log.i(TAG, "Position: " + position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    /**
     * ViewPager 어댑터
     */
    public class CustomAdapter extends PagerAdapter {

        LayoutInflater mInflater;
        Context mContext;


        public CustomAdapter(Context context){
            mContext = context;
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return ITEM_COUNT;
        }


        @Override
        public boolean isViewFromObject(View view, Object object) {

            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public float getPageWidth(int position) {
            return super.getPageWidth(position);
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            int virtualPosition = position % ITEM_COUNT;
            View view = null; // 가이드화면 3,4,5에서 사용
            View view1 = null; // 가이드화면 1에서 사용
            View view2 = null; // 가이드화면 2에서 사용

            Log.i(TAG, "instantiateItem: position: "+ position);
            final ImageView mViewpagerImg;
            final LinearLayout mBottomWhite;

            view1 = mInflater.inflate(R.layout.prologue_1, null);
            view2 = mInflater.inflate(R.layout.prologue_2, null);
            view = mInflater.inflate(R.layout.prologue_3, null);

            layoutTop1 = view1.findViewById(R.id.layoutTop);
            layoutTop2 = view2.findViewById(R.id.layoutTop);
            textChange_1 = view.findViewById(R.id.text_top);
            textChange_2 = view.findViewById(R.id.text_bottom);

            imgView = view.findViewById(R.id.guideImage);
            layoutTop = view.findViewById(R.id.layoutTop);


            //get screen size
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int height = size.y;

            switch(position){
                case 0: // 첫번째 프롤로그 화면

                    layoutTop1.setPadding(0,0,0,height*3/5);
                    container.addView(view1);

                    return view1;

                case 1: // 두번째 프롤로그 화면

                    layoutTop2.setPadding(0,0,0,height*2/5);
                    container.addView(view2);

                    return view2;

                case 2:  // 세번째 프롤로그 화면

                    textChange_1.setText("라이브");
                    textChange_2.setText("24시간 지속하는 \n 라이브 방송을 쉽고 편하게 시청");
                    imgView.setImageResource(R.drawable.guide_3);

                    container.addView(view);

                    break;

                case 3:  // 세번째 프롤로그 화면

                    textChange_1.setText("주요뉴스");
                    textChange_2.setText("주요뉴스와 최신뉴스를 \n심플하고 시원해진 화면에서 한 눈에");
                    imgView.setImageResource(R.drawable.guide_4);

                    container.addView(view);

                    break;

                case 4:  // 네번째 프롤로그 화면
                    textChange_1.setText("디지털 뉴스");
                    textChange_2.setText("TV에서는 볼 수 없었던 \n연합뉴스TV의 고품 디지털 뉴스까지!");
                    imgView.setImageResource(R.drawable.guide_5);

                    container.addView(view);

                    break;


            }

//            container.addView(view);

            return view;
        }
    }


    private void goMain() {
        //splash에서 encoded token 받아오기
        encodedToken = getIntent().getStringExtra("encoded");

        receivedUrl = getIntent().getStringExtra("pushUrl");

        Intent intent = new Intent(GuideActivity.this, MainActivity.class);
        intent.putExtra("encoded", encodedToken);

            //if push url exist open it in MainActivity
        if (getIntent() != null) {
            String getUrl = getIntent().getStringExtra("pushUrl");
            if (getUrl != null) {
                Log.i(TAG, "Received push url from Splash to Guide: " + getUrl);
                intent.putExtra("pushUrl", getUrl);
            }
        }

        startActivity(intent);
        finish();
    }


}

