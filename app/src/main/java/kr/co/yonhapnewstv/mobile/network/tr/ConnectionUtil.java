package kr.co.yonhapnewstv.mobile.network.tr;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import kr.co.yonhapnewstv.mobile.util.JsonLogPrint;
import kr.co.yonhapnewstv.mobile.util.Logger;


public class ConnectionUtil {
    private final String TAG = ConnectionUtil.class.getSimpleName();

    private String mUrl;
    private StringBuffer paramSb = new StringBuffer();

    public ConnectionUtil(String url) {
        mUrl = url;
    }


    public String getParam() {
        String params = paramSb.toString();
        if ("".equals(params.trim()) == false)
            params = params.substring(0, params.length() - 1);

        return params;
    }

    //NEW 통신 모듈
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String doConnection(JSONObject body, String tr, String Url) {
        URL mURL;
        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection = null;
        String result = "";
        try {

            URL url = new URL(Url);

            String urlParameters = "";
            for(int i = 0; i<body.names().length(); i++){
                if(i == 0){
                    urlParameters += body.names().getString(i) + "=" +  body.get(body.names().getString(i));
                }
                else{
                    urlParameters += "&"+ body.names().getString(i) + "=" +  body.get(body.names().getString(i));
                }
            }

            Log.v(TAG, urlParameters);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(15 * 1000);
            urlConnection.setReadTimeout(15 * 1000);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.connect();
            //You Can also Create JSONObject here
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());

            if (body != null) {
                String params = URLEncoder.encode("strJson="+body.toString(), "utf-8");

                out.write(urlParameters);

                JsonLogPrint.printJson(body.toString());
                Logger.i(TAG, "encordingParams="+params);
            } else {
                Logger.e(TAG, "Param is Null");
            }
            out.close();
            int HttpResult = urlConnection.getResponseCode();
            Log.i(TAG,  "HttpResult="+HttpResult);
            if (HttpResult == HttpURLConnection.HTTP_OK || HttpResult == HttpURLConnection.HTTP_SERVER_ERROR) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                result = sb.toString().replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<string xmlns=\"http://tempuri.org/\">", "").replace("</string>", "");

                Log.e("new Test", "" + result);
                return result;
            } else {
                Log.e(" ", "" + urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return null;
    }


}
