package kr.co.yonhapnewstv.mobile.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextWatcherFloatUtil {
    private String beforeText = "";
    public void setTextWatcher(final EditText editText, final float maxVal, final int dotAfterCnt) {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String str = s.toString();
                if (beforeText.equals(str)) {
                    if (str.equals("") == false)
                        editText.setSelection(str.length());
                    return;
                }

                if (str.length() != 0) {
                    float val = StringUtil.getFloat(s.toString());

                    if (val == 0 || val > maxVal) {
                        str = str.substring(0, str.length()-1);
                        beforeText = str;
                        editText.setText(str);
                    }

                    String[] dotAfter = str.split("\\.");
                    if (dotAfter.length >= 2 && (dotAfter[1].length() > dotAfterCnt)) {
                        str = str.substring(0, str.length()-1);
                        beforeText = str;
                        editText.setText(str);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        };

        editText.addTextChangedListener(textWatcher);
    }
}
