package kr.co.yonhapnewstv.mobile.network.tr;

import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.RequiresApi;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.yonhapnewstv.mobile.util.JsonLogPrint;
import kr.co.yonhapnewstv.mobile.util.Logger;

public class ApiData {
	private final String	TAG			= ApiData.class.getSimpleName();
	public static final int	TYPTE_NONE	= -1;

	private int				trMode		= -1;
	private String Url;

    //NEW 통신 모듈
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public Object getData(BaseData baseData, Object obj) {
        JSONObject body = null;

        try {
            body = baseData.makeJson(obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Url = baseData.conn_url;
        Logger.d(TAG, "baseData.conn_url="+baseData.conn_url);

        Logger.i(TAG, "ApiData.url="+baseData.conn_url);
        ConnectionUtil connectionUtil = new ConnectionUtil(baseData.conn_url);


        String result = null;
        if (body != null) {
            result = connectionUtil.doConnection(body, baseData.getClass().getSimpleName(), Url);

        }

        if (TextUtils.isEmpty(result)) {
            Logger.e(TAG, "getData.result="+result);
        } else {
            Logger.i(TAG, "####################### API RESULT."+baseData.getClass().getSimpleName()+" #####################");
            JsonLogPrint.printJson(result);
            Logger.i(TAG, "####################### API RESULT."+baseData.getClass().getSimpleName()+" #####################");
        }


        Gson gson = new Gson();
        if(result.startsWith("[")) {
            // json 배열 처리
            return baseData.gsonFromArrays(gson, result);
        }else{
            // json 단일 처리
            return gson.fromJson(result, baseData.getClass());
        }
    }



	public interface IStep {
		void next(Object obj);
	}

    public interface IFailStep {
        void fail();
    }
}
