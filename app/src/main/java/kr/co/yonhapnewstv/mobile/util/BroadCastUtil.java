package kr.co.yonhapnewstv.mobile.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import kr.co.yonhapnewstv.mobile.R;


public class BroadCastUtil extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Intent notificationIntent = new Intent(context.getApplicationContext(), SplashActivity.class);
        //notificationIntent.putExtra("notificationIds", notfi_id); //전달할 값//

        PackageManager pk = context.getPackageManager();

        Intent notificationIntent = pk.getLaunchIntentForPackage(context.getPackageName());

        String title = intent.getExtras().getString("push_title");
        String subtitle = intent.getExtras().getString("push_subtitle");
        String index = intent.getExtras().getString("push_index");
        String param1 = intent.getExtras().getString("push_param1");
        String param2 = intent.getExtras().getString("push_param2");

        Log.d("AlarmUtil", "PushSet - title : " + title + " / subtitle : " + subtitle + " / index : " + index + " / param1 : " + param1 + " / param2 : " + param2);

        notificationIntent.putExtra("push_index", index); //전달할 값*/
        notificationIntent.putExtra("push_param1", param1); //전달할 값*/
        notificationIntent.putExtra("push_param2", param2); //전달할 값*/
        //notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendnoti = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        //PendingIntent contentIntent = PendingIntent.getActivity(this, (int)(System.currentTimeMillis()/1000), notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        String channelId = "channel";
        String channelName = "Channel Name";
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);

            notificationManager.createNotificationChannel(mChannel);

            builder = new NotificationCompat.Builder(context.getApplicationContext(), channelId);

        }else{
            builder = new NotificationCompat.Builder(context);
        }

        builder.setContentTitle(title)
                .setContentText(subtitle)
                .setTicker(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setContentIntent(pendnoti)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_ALL);

        /* if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) { builder.setCategory(Notification.CATEGORY_MESSAGE) .setPriority(Notification.PRIORITY_HIGH) .setVisibility(Notification.VISIBILITY_PUBLIC); } */
        notificationManager.notify(1, builder.build());

        Log.d("Tag", "Send Alarm!!!!!!!!!!!!!!!!!!!!!!!!");
    }
}
