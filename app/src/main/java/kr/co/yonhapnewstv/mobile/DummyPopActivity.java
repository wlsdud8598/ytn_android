package kr.co.yonhapnewstv.mobile;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import kr.co.yonhapnewstv.mobile.push.NotiDummyActivity;
import kr.co.yonhapnewstv.mobile.util.Util;

/**
 * Created by mrsohn on 2017. 3. 6..
 */

public class DummyPopActivity extends BaseActivity {
    private static final String TAG = DummyPopActivity.class.getSimpleName();

    private Button okBtn, noBtn;
    private TextView msg;

    private Intent intent;
    private String url_link = "";
    private String popup_msg = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.noti_pop);

        intent = getIntent();
        if (intent != null){
            url_link = getIntent().getStringExtra(NotiDummyActivity.NOTI_LINK);
            popup_msg = getIntent().getStringExtra(NotiDummyActivity.NOTI_BODY);
        }

        if(url_link == null || url_link.equals("")){
            url_link = "";
        }

        msg = findViewById(R.id.dialog_content_tv);

        okBtn = findViewById(R.id.dialog_btn_ok);
        noBtn = findViewById(R.id.dialog_btn_no);


        msg.setText(popup_msg);

        okBtn.setOnClickListener(mOnClickListener);
        noBtn.setOnClickListener(mOnClickListener);




    }

    //클릭 이벤트 정의
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewId = v.getId();
            NotificationManager notifiyMgr = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

            switch (viewId) {
                case R.id.dialog_btn_ok :
                    notifiyMgr.cancelAll();
                    movePop();
                    break;
                case R.id.dialog_btn_no :
                    finish();
                    overridePendingTransition(0, 0); // 애니메이션 없애기 코드
                    break;
            }
        }
    };


    private void movePop(){

        String mainActivity = MainActivity.class.getName();
        boolean isExcuteApp = Util.getServiceTaskName(this, mainActivity); // 앱 실행 중 여부

        if (isExcuteApp) {
            NotiDummyActivity.moveToMenu(DummyPopActivity.this, url_link);

        } else {
            Intent intentSplash = new Intent(DummyPopActivity.this, SplashActivity.class);
            intentSplash.putExtra("pushUrl", url_link);
            startActivity(intentSplash);
        }

        finish();
        overridePendingTransition(0, 0); // 애니메이션 없애기 코드
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        url_link = intent.getStringExtra(NotiDummyActivity.NOTI_LINK);
        popup_msg = intent.getStringExtra(NotiDummyActivity.NOTI_BODY);

        Log.d(TAG,"popup_msg  : "+popup_msg + ", url_link : " + url_link);

        if(url_link == null || url_link.equals("")){
            url_link = "";
        }


        msg.setText(popup_msg);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, TAG+".onStop()");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, TAG+".onDestroy()");
    }



}
