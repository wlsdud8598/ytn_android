package kr.co.yonhapnewstv.mobile.push;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import kr.co.yonhapnewstv.mobile.BaseActivity;
import kr.co.yonhapnewstv.mobile.DummyPopActivity;
import kr.co.yonhapnewstv.mobile.GuideActivity;
import kr.co.yonhapnewstv.mobile.MainActivity;
import kr.co.yonhapnewstv.mobile.R;
import kr.co.yonhapnewstv.mobile.SplashActivity;
import kr.co.yonhapnewstv.mobile.component.PushUtils;
import kr.co.yonhapnewstv.mobile.util.SharedPref;
import kr.co.yonhapnewstv.mobile.util.Util;

import static android.content.Intent.FLAG_ACTIVITY_NO_ANIMATION;


public class NotiDummyActivity extends Activity {
    public static final String TAG = NotiDummyActivity.class.getSimpleName();
    public static final String NOTI_LINK = "NOTI_LINK";
    public static final String NOTI_TITLE = "NOTI_TITLE";
    public static final String NOTI_BODY = "NOTI_BODY";

    public static Intent popIntent;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPref.getInstance().initContext(this);

        if(BaseActivity.foregroundCount <= 1){
            Intent intent = new Intent(NotiDummyActivity.this, DummyPopActivity.class);
            startActivity(intent);
//            finish();
        }


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        String notiLink = "";
        String notiTitle = "";
        String notiBody = "";

        if (getIntent() == null) {

        } else {
            notiLink = getIntent().getStringExtra(NOTI_LINK);
            notiTitle = getIntent().getStringExtra(NOTI_TITLE);
            notiBody = getIntent().getStringExtra(NOTI_BODY);
        }


        Log.i(TAG, "dummyacvty notiLink=" + notiLink);
        Log.i(TAG, "dummyacvty notiTitle=" + notiTitle);
        Log.i(TAG, "dummyacvty notiBody=" + notiBody);

        String mainActivity = MainActivity.class.getName();
        boolean isExcuteApp = Util.getServiceTaskName(this, mainActivity); // 앱 실행 중 여부


        if (isExcuteApp) {
            moveToMenu(this, notiLink);

        } else {
            Intent intentSplash = new Intent(this, SplashActivity.class);
            intentSplash.putExtra("pushUrl", notiLink);
            startActivity(intentSplash);
        }

        finish();
        overridePendingTransition(0, 0); // 애니메이션 없애기 코드
    }

    /**
     *이동 처리
     */
    public static void moveToMenu(Activity activity, String link) {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.putExtra("pushUrl", link);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        activity.startActivity(intent);
    }

    /**
     * 푸시 메시지를 전달 받으면 상태표시바에 표시함
     */
    public static void showPushMessage(Context context, String title, String body, String url) {

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        PushUtils.releaseWakeLock();

        Intent notificationIntent = new Intent(context, NotiDummyActivity.class);

        notificationIntent.putExtra(NotiDummyActivity.NOTI_LINK, url);
        notificationIntent.putExtra(NotiDummyActivity.NOTI_TITLE, title);
        notificationIntent.putExtra(NotiDummyActivity.NOTI_BODY, body);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context, (int) (System.currentTimeMillis() / 1000), notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Builder builder;// = new NotificationCompat.Builder(this);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ID = context.getString(R.string.app_name);
            String CHANNEL_NAME = context.getString(R.string.app_name);
            @SuppressLint("WrongConstant")
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_HIGH);
            channel.enableLights(false);
            channel.enableVibration(true);

            notificationManager.createNotificationChannel(channel);

            builder = new NotificationCompat.Builder(context, CHANNEL_ID);
        } else {
            builder = new NotificationCompat.Builder(context);
        }

        String notiTitle = TextUtils.isEmpty(title) ? context.getString(R.string.app_name) : title;

        builder.setContentTitle(notiTitle)   // 상태바 드래그시 보이는 타이틀
                .setContentText(body)                                // 상태바 드래그시 보이는 서브타이틀
                .setTicker(body)                                     // 상태바 한줄 메시지
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setContentIntent(contentIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_ALL);

//        int noti_id = (int)(System.currentTimeMillis()/1000);
//        nm.notify(TAG, noti_id, builder.build());
        nm.notify(TAG, 1, builder.build());

        String mainActivity = MainActivity.class.getName();
        boolean isMainApp = Util.getServiceTaskName(context, mainActivity); // 앱 실행 중 여부

        String guideActivity = GuideActivity.class.getName();
        boolean isGuideApp = Util.getServiceTaskName(context, guideActivity); // 앱 실행 중 여부

        String splashActivity = SplashActivity.class.getName();
        boolean isSplashApp = Util.getServiceTaskName(context, splashActivity); // 앱 실행 중 여부

        if(isSplashApp){
            if(BaseActivity.foregroundCount <= 0) {
                SplashActivity.activity.finish();
            }
        } else if(isGuideApp){
            if(BaseActivity.foregroundCount <= 0) {
                GuideActivity.activity.finish();
            }
        } else if(isMainApp){
            if(BaseActivity.foregroundCount <= 0) {
                MainActivity.activity.finish();
            }
        }

        popIntent = new Intent(context, DummyPopActivity.class);
        popIntent.addFlags(FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        popIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        popIntent.putExtra(NOTI_LINK, url);
        popIntent.putExtra(NOTI_BODY, body);


        context.startActivity(popIntent);






    }

}
