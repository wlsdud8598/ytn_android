package kr.co.yonhapnewstv.mobile.component;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import kr.co.yonhapnewstv.mobile.R;
import kr.co.yonhapnewstv.mobile.util.Logger;


/**
 * 공통 메시지 다이얼로그
 */
public class CDialog extends Dialog {
    private static final String TAG = CDialog.class.getSimpleName();
    private boolean mIsAutoDismiss = true;

    private LinearLayout mTitleLayout, parent_dialog_layout;
    private TextView mTitleView;
    private TextView mMessageView;
    private Button mNoButton;
    private Button mOkButton;
    private String mTitle;
    private String mMessage;
    private LinearLayout mContentLayout;
    private LinearLayout mBtnLayout;

    private View.OnClickListener mNoClickListener;
    private View.OnClickListener mOkClickListener;
    private static DismissListener mDismissListener;

    private static CDialog instance;

    private static CDialog getInstance(Context context) {
        instance = new CDialog(context);
        initDialog(instance);
        return instance;
    }

    private static CDialog getInstance(Activity activity) {
        instance = new CDialog(activity);
        initDialog(instance);
        return instance;
    }

    private static void initDialog(CDialog instance) {
        if (instance == null) {
            return;
        }

        if (instance.mOkButton != null) {
            instance.mOkButton.setVisibility(View.GONE);
            instance.mOkButton.setOnClickListener(null);
        }

        if (instance.mNoButton != null) {
            instance.mNoButton.setVisibility(View.GONE);
            instance.mNoButton.setOnClickListener(null);
        }

        if (instance.mBtnLayout != null)
            instance.mBtnLayout.setVisibility(View.GONE);

        if (mDismissListener != null)
            mDismissListener = null;

        if (instance.mTitleView != null)
            instance.mTitleView.setText(instance.getContext().getString(R.string.text_alert));

        instance.setOnDismissListener(null);

        if (instance.isShowing() == false)
            instance.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);


        setContentView(R.layout.custom_dialog);
        setLayout();
        setClickListener();
    }

    public CDialog(Context context) {
        // Dialog 배경을 투명 처리 해준다.
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
    }

    public static CDialog showDlg(Context context, String message) {
        final CDialog dlg = getInstance(context);
        dlg.setMessage(message);
        dlg.mOkButton.setVisibility(View.VISIBLE);
        dlg.mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });
        return dlg;
    }


    public static CDialog showDlg(Context context, String message, int minWidth) {
        final CDialog dlg = getInstance(context);
        dlg.setMessage(message);
        dlg.mOkButton.setVisibility(View.VISIBLE);
        dlg.mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });

        FrameLayout.LayoutParams params1 = (FrameLayout.LayoutParams) dlg.parent_dialog_layout.getLayoutParams();
        params1.width = minWidth;
        dlg.parent_dialog_layout.setLayoutParams(params1);

        return dlg;
    }

    public static CDialog showDlg(Context context, String message, boolean canelable) {
        final CDialog dlg = getInstance(context);
        dlg.setMessage(message);
        dlg.mOkButton.setVisibility(View.VISIBLE);
        dlg.mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });
        dlg.setCancelable(canelable);
        return dlg;
    }

    public static CDialog LoginshowDlg(Context context, String title, String message) {
        CDialog dlg = getInstance(context);
        dlg.setTitle(title);
        dlg.setMessage(message);

        return dlg;
    }

    public static CDialog showDlg(Context context, String title, String message) {
        CDialog dlg = getInstance(context);
        dlg.setTitle(title);
        dlg.setMessage(message);

        return dlg;
    }

    public static CDialog showDlg(Context context, String message, View.OnClickListener okListener) {
        CDialog dlg = getInstance(context);
        dlg.setMessage(message);
        dlg.setOkButton(okListener);

        return dlg;
    }

    public static CDialog showDlg(Context context, String title, String message, View.OnClickListener okListener) {
        CDialog dlg = getInstance(context);
        dlg.setTitle(title);
        dlg.setMessage(message);
        dlg.setOkButton(okListener);

        return dlg;
    }

    public static CDialog showDlg(Activity activity, String message, final CDialog.DismissListener dismissListener) {
        final CDialog dlg = getInstance(activity);
        mDismissListener = dismissListener;
        dlg.setMessage(message);
        dlg.mOkButton.setVisibility(View.VISIBLE);
        dlg.mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });

        return dlg;
    }

    public static CDialog showDlg(Context context, String message, final CDialog.DismissListener dismissListener) {
        final CDialog dlg = getInstance(context);
        mDismissListener = dismissListener;
        dlg.setMessage(message);
        dlg.mOkButton.setVisibility(View.VISIBLE);
        dlg.mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });

        return dlg;
    }

    public static CDialog showDlg(Context context, String message, View.OnClickListener okListener, View.OnClickListener noListener) {
        CDialog dlg = getInstance(context);
        dlg.setMessage(message);

        dlg.setOkButton(okListener);
        dlg.setNoButton(noListener);

        return dlg;
    }

    public static CDialog CallshowDlg(Context context, String message, View.OnClickListener okListener, View.OnClickListener noListener) {
        CDialog dlg = getInstance(context);
        dlg.setMessage(message);
        dlg.mOkButton.setText("고객센터\n연결하기");
        dlg.mNoButton.setText("확인");
        dlg.setOkButton(okListener);
        dlg.setNoButton(noListener);

        return dlg;
    }

    public static CDialog UpdateshowDlg(Context context, String message, View.OnClickListener okListener, View.OnClickListener noListener) {
        CDialog dlg = getInstance(context);
        dlg.setMessage(message);
        dlg.mOkButton.setText("확인");
        dlg.setOkButton(okListener);
        dlg.setNoButton(noListener);

        return dlg;
    }


    public static CDialog showDlg(Context context, View view) {
        CDialog dlg = getInstance(context);
        dlg.mContentLayout.removeAllViews();
        dlg.mContentLayout.addView(view);
        dlg.mContentLayout.setPadding(0,0,0,0);
        dlg.mOkButton.setVisibility(View.GONE);
        dlg.mBtnLayout.setVisibility(View.GONE);
        return dlg;
    }

    public static CDialog showDlg(Context context, View view, LinearLayout.LayoutParams params) {
        CDialog dlg = getInstance(context);
        dlg.mContentLayout.removeAllViews();
        dlg.mContentLayout.addView(view,params);
        dlg.mContentLayout.setPadding(0,0,0,0);
        dlg.mOkButton.setVisibility(View.GONE);
        dlg.mBtnLayout.setVisibility(View.GONE);

        return dlg;
    }

    public static CDialog showDlg(Context context, View view, boolean margin) {
        CDialog dlg = getInstance(context);
        dlg.mContentLayout.removeAllViews();
        dlg.mContentLayout.addView(view);
        dlg.mContentLayout.setPadding(0,0,0,0);
        if(margin) {
            LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) dlg.mContentLayout.getLayoutParams();
            params2.topMargin = 0;
            dlg.mContentLayout.setLayoutParams(params2);
        }
        dlg.mOkButton.setVisibility(View.GONE);
        dlg.mBtnLayout.setVisibility(View.GONE);

        return dlg;
    }


    public static CDialog showDlg(Context context, View view, int width) {
        CDialog dlg = getInstance(context);
        dlg.mContentLayout.removeAllViews();
        dlg.mContentLayout.addView(view);
        dlg.mContentLayout.setPadding(0,0,0,0);

        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) dlg.mContentLayout.getLayoutParams();
        params2.topMargin = 0;
        dlg.mContentLayout.setLayoutParams(params2);

        FrameLayout.LayoutParams params1 = (FrameLayout.LayoutParams) dlg.parent_dialog_layout.getLayoutParams();
        params1.width = width;
        dlg.parent_dialog_layout.setLayoutParams(params1);

        dlg.mOkButton.setVisibility(View.GONE);
        dlg.mBtnLayout.setVisibility(View.GONE);

        return dlg;
    }


    public static CDialog showDlg(Context context, View view, int minHeight, int minWidth, boolean autodismiss) {
        CDialog dlg = getInstance(context);
        dlg.mContentLayout.removeAllViews();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(minWidth, minHeight);
        dlg.mContentLayout.addView(view,params);
        dlg.mContentLayout.setPadding(0,0,0,0);
        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) dlg.mContentLayout.getLayoutParams();
        params2.topMargin = 0;
        dlg.mContentLayout.setLayoutParams(params2);

        FrameLayout.LayoutParams params1 = (FrameLayout.LayoutParams) dlg.parent_dialog_layout.getLayoutParams();
        params1.width = minWidth;
        dlg.parent_dialog_layout.setLayoutParams(params1);

        dlg.mOkButton.setVisibility(View.GONE);
        dlg.mBtnLayout.setVisibility(View.GONE);

        dlg.mIsAutoDismiss = autodismiss;

        return dlg;
    }




    public static CDialog showDlg(Context context, int layout) {
        View view = LayoutInflater.from(context).inflate(layout, null);
        CDialog dlg = getInstance(context);
        dlg.mContentLayout.removeAllViews();
        dlg.mContentLayout.addView(view);
        dlg.mContentLayout.setPadding(0,0,0,0);
        dlg.mOkButton.setVisibility(View.GONE);
        dlg.mBtnLayout.setVisibility(View.GONE);

        return dlg;
    }

    public static CDialog showDlg(Context context, View view, View.OnClickListener okListener, View.OnClickListener noListener) {
        CDialog dlg = getInstance(context);
        dlg.setContentView(view);

        dlg.setOkButton(okListener);
        dlg.setNoButton(noListener);

        return dlg;
    }

    public static CDialog showDlg(Context context, String title, String message, View.OnClickListener okListener, View.OnClickListener noListener) {
        CDialog dlg = getInstance(context);

        dlg.setTitle(title);
        dlg.setMessage(message);
        dlg.setOkButton(okListener);
        dlg.setNoButton(noListener);

        return dlg;
    }

    /**
     * 타이틀 세팅
     * 타이틀이 없으면 타이틀 영역을 Gone 처리
     * @param title
     */
    public CDialog setTitle(String title) {
        if (TextUtils.isEmpty(title)) {
            mTitleLayout.setVisibility(View.GONE);
            mTitleView.setVisibility(View.GONE);
        } else {
            mTitleLayout.setVisibility(View.VISIBLE);
            mTitleView.setVisibility(View.VISIBLE);
            mTitleView.setText(title);
        }
        return instance;
    }



    /**
     * 타이틀 세팅
     * 타이틀이 없으면 타이틀 영역을 Gone 처리
     * @param title
     */
    public void setTitle(String title, boolean food) {
        if (TextUtils.isEmpty(title)) {
            mTitleLayout.setVisibility(View.GONE);
        } else {
            mTitleLayout.setVisibility(View.VISIBLE);
            mTitleView.setText(title);

            if(food){
                mTitleView.setVisibility(View.VISIBLE);
                mTitleLayout.setBackgroundColor(getContext().getResources().getColor(R.color.x245_245_245));

            }
        }
    }


    public CDialog setMessage(String message) {
        mMessageView.setText(message);
        return instance;
    }

    public CDialog setMessageGravity(int gravity) {
        mMessageView.setGravity(gravity);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mMessageView.getLayoutParams();
        params.gravity = gravity;
        mMessageView.setLayoutParams(params);
        return instance;
    }

    public CDialog setOneBtn(int src) {
        mOkButton.setBackgroundResource(src);
        mOkButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorWhite));
        return instance;
    }

    public CDialog setClickListener() {
        Logger.i("", "setClickListener=" + mNoButton);

        if (mNoClickListener == null) {
            mNoButton.setVisibility(View.GONE);
        }

        mNoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mNoClickListener != null) {
                    mNoClickListener.onClick(v);

                    if (mIsAutoDismiss) {
                        CDialog.this.dismiss();
                    }
                } else {
                    CDialog.this.dismiss();
                }
            }
        });

        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOkClickListener != null) {
                    mOkClickListener.onClick(v);

                    if (mIsAutoDismiss) {
                        CDialog.this.dismiss();
                    }
                } else {
                    CDialog.this.dismiss();
                }
            }
        });

        return instance;
    }

    /**
     * 왼쪽 버튼 세팅
     */
    public CDialog setNoButton(View.OnClickListener noClickListener) {
        mBtnLayout.setVisibility(View.VISIBLE);
        mNoButton.setVisibility(View.VISIBLE);
        setAlertButtonClickListener(mNoButton, noClickListener);
        mOkButton.setBackgroundResource(R.drawable.draw_alert_ok_btn);
        mOkButton.setTextColor(ContextCompat.getColor(getContext(), R.color.colorWhite));
        return instance;
    }

    public CDialog setNoButton(String label, View.OnClickListener noClickListener) {
//        mBtnLayout.setVisibility(View.VISIBLE);
//        mNoButton.setVisibility(View.VISIBLE);
//        viewTerm.setVisibility(View.VISIBLE);
        mNoButton.setText(label);
//        setAlertButtonClickListener(mNoButton, noClickListener);
        return setNoButton(noClickListener);
    }

    public CDialog setNoButtonDismiss(String label, OnDismissListener dismissListener) {
//        mBtnLayout.setVisibility(View.VISIBLE);
//        mNoButton.setVisibility(View.VISIBLE);
//        viewTerm.setVisibility(View.VISIBLE);
        mNoButton.setText(label);
//        setAlertButtonClickListener(mNoBtton, noClickListener);
        setOnDismissListener(dismissListener);
        return instance;
    }

    /**
     * 오른쪽 버튼 세팅
     *
     * @param okClickListener
     */
    public CDialog setOkButton(View.OnClickListener okClickListener) {
        mBtnLayout.setVisibility(View.VISIBLE);
        String label = mOkButton.getText().toString();
        setOkButton(label, okClickListener);
        return instance;
    }

    public CDialog setOkButton(String label, final View.OnClickListener okClickListener) {
        this.mOkClickListener = okClickListener;
        mBtnLayout.setVisibility(View.VISIBLE);
        mOkButton.setVisibility(View.VISIBLE);
        mOkButton.setText(label);
        setAlertButtonClickListener(mOkButton, okClickListener);
        return instance;
    }

    public void setOkButtonDissmissListenr(Button button, View.OnClickListener clickListener) {
        this.mOkClickListener = clickListener;
    }

    public void setNoButtonDissmissListenr(Button button, View.OnClickListener clickListener) {
        button.hasOnClickListeners();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (mDismissListener != null)
            mDismissListener.onDissmiss();
    }

    /**
     * 뒤로가기 단, 버튼이 1개인 다이얼로그는 button click으로 인지한다.
     */
    @Override
    public void onBackPressed() {
        if(mOkButton.getVisibility() == View.VISIBLE && mNoButton.getVisibility() == View.VISIBLE){

        }else{
            super.onBackPressed();
        }
    }

    public interface DismissListener {
        void onDissmiss();
    }

    private void setAlertButtonClickListener(Button button, final View.OnClickListener clickListener) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.onClick(v);

                    if (mIsAutoDismiss) {
                        CDialog.this.dismiss();
                    }
                } else {
                    CDialog.this.dismiss();
                }
            }
        });
    }

    /*
     * Layout
     */
    public void setLayout() {
        mTitleLayout = findViewById(R.id.dialog_title_layout);
        mTitleView = (TextView) findViewById(R.id.dialog_title);
        mMessageView = (TextView) findViewById(R.id.dialog_content_tv);
        mNoButton = (Button) findViewById(R.id.dialog_btn_no);
        mOkButton = (Button) findViewById(R.id.dialog_btn_ok);
        mContentLayout = (LinearLayout) findViewById(R.id.dialog_content_layout);
        mBtnLayout = (LinearLayout) findViewById(R.id.dialog_btn_layout);
        parent_dialog_layout = findViewById(R.id.parent_dialog_layout);

    }


    public void setBackgroundOkBtn(int background){

        if(mOkButton!=null)
            mOkButton.setBackgroundResource(background);


    }
    public void setBackgroundNoBtn(int background){

        if(mNoButton!=null)
            mNoButton.setBackgroundResource(background);

    }

    public void setTextColorNoBtn(int color){

        if(mNoButton!=null)
            mNoButton.setTextColor(color);

    }

    public void setTextColorOkBtn(int color){

        if(mOkButton!=null)
            mOkButton.setTextColor(color);

    }
}