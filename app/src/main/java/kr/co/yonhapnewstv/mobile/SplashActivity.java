package kr.co.yonhapnewstv.mobile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.security.SecureRandom;
import java.util.Random;

import kr.co.yonhapnewstv.mobile.component.CDialog;
import kr.co.yonhapnewstv.mobile.network.tr.ApiData;
import kr.co.yonhapnewstv.mobile.network.tr.BaseData;
import kr.co.yonhapnewstv.mobile.network.tr.CConnAsyncTask;
import kr.co.yonhapnewstv.mobile.network.tr.data.Tr_Token;
import kr.co.yonhapnewstv.mobile.util.Logger;
import kr.co.yonhapnewstv.mobile.util.NetworkUtil;
import kr.co.yonhapnewstv.mobile.util.SharedPref;

public class SplashActivity extends BaseActivity {
    protected final String TAG = getClass().getSimpleName();
    public static SplashActivity activity = null;    //액티비티 변수 선언

    private String token = "";
    private String encodedToken = "";
    private String random = "";
    private String neverOpen = "";


    private final Random RANDOM = new SecureRandom();
    private final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        SharedPref.getInstance().initContext(this);

        activity = this;

        if (NetworkUtil.getConnectivityStatus(SplashActivity.this) == false) {
            CDialog.showDlg(this, getString(R.string.text_network_error), new CDialog.DismissListener() {
                @Override
                public void onDissmiss() {
                    finish();
                }
            });
            return;
        }

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        token = task.getResult().getToken();

                        //if dk not exist, create random string with same token length and store in sharedpref
                        random = SharedPref.getInstance().getPreferences(SharedPref.USER_VAL);
                        if(random.equals("")){
                            random = generateRandomString(token.length());
                            SharedPref.getInstance().savePreferences(SharedPref.USER_VAL, random);

                            Log.d(TAG, "USER_VAL: "+SharedPref.getInstance().getPreferences(SharedPref.USER_VAL));

                        }

                        // send token to the endpoint (domain/pushNotification)


                        // Log and toast
                        String msg = token;
                        SharedPref.getInstance().savePreferences(SharedPref.TOKEN, token);
                        Log.d(TAG, "token: "+SharedPref.getInstance().getPreferences(SharedPref.TOKEN));

                        String msg2 = random;
                        Log.d(TAG, "random dk: "+msg2);



                        Handler mHandler = new Handler(Looper.getMainLooper());
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ytn();

                            }
                        }, 300);
                    }
                });
    }


    private String generateRandomString(int length) {
        StringBuilder returnValue = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }
        return new String(returnValue);
    }

    //서버 통신 함수
    private void ytn() {

        Tr_Token tr = new Tr_Token();
        Tr_Token.RequestData reqData = new Tr_Token.RequestData();

        if(SharedPref.getInstance().getPreferences(SharedPref.PUSH_NOTI) != ""){
            reqData.pn = SharedPref.getInstance().getPreferences(SharedPref.PUSH_NOTI);
        }
        else{
            reqData.pn = "Y";
        }
        reqData.dk = random;
        reqData.pk = token;
        reqData.pf = "android";


        getData(tr, reqData, new ApiData.IStep() {
            @Override
            public void next(Object obj) {

                if (obj instanceof Tr_Token) {
                    Tr_Token data = (Tr_Token) obj;
                    if (data.result.equals("ok")) {
//                        SharedPref.getInstance().savePreferences(SharedPref.TOKEN_ENCRYPTED, data.token);

                        encodedToken = data.token;
                        Log.i(TAG, "ENCODED TOKEN: " + data.token);
                        goMain();
                    }
                }
            }
        });
    }

    private void goMain() {
        //shared pref neverOpen = no, 가이드 화면 거쳤다가
        neverOpen = SharedPref.getInstance().getPreferences(SharedPref.NEVER_OPEN);
        Log.i(TAG, "neverOpen value: "+ neverOpen);


        if(neverOpen.equals("")) {
            Intent intent = new Intent(SplashActivity.this, GuideActivity.class);
            intent.putExtra("encoded", encodedToken);
            //if push url exist open it in MainActivity
            if (getIntent() != null) {
                String getUrl = getIntent().getStringExtra("pushUrl");
                if (getUrl != null) {
                    Log.i(TAG, "Received push url from NotiDummy to Splash: " + getUrl);
                    intent.putExtra("pushUrl", getUrl);
                }
            }
            startActivity(intent);
            finish();
        }

        //shared pref neverOpen = yes, 바로 메인으로
        else if(neverOpen.equals("yes")) {
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            intent.putExtra("encoded", encodedToken);

            //if push url exist open it in MainActivity
            if (getIntent() != null) {
                String getUrl = getIntent().getStringExtra("pushUrl");
                if (getUrl != null) {
                    Log.i(TAG, "Received push url from NotiDummy to Splash: " + getUrl);
                    intent.putExtra("pushUrl", getUrl);
                }
            }

            startActivity(intent);
            finish();
        }
    }

    //Destroy splash activity after it goes to next activity
    @Override
    protected void onPause()
    {
        super.onPause();
        overridePendingTransition(0, 0);

        finish();
    }

    // 서버 통신
    public void getData(final BaseData cls, final Object obj, final ApiData.IStep step) {
        if (NetworkUtil.getConnectivityStatus(SplashActivity.this) == false) {
            CDialog.showDlg(this, getString(R.string.text_network_error), new CDialog.DismissListener() {
                @Override
                public void onDissmiss() {
                    finish();
                }
            });
            return;
        }

        CConnAsyncTask.CConnectorListener queryListener = new CConnAsyncTask.CConnectorListener() {
            @Override
            public Object run() {
                ApiData data = new ApiData();
                return data.getData(cls, obj);
            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void view(CConnAsyncTask.CQueryResult result) {
                Logger.i(TAG, "result.result=" + result.result + ", result.data=" + result.data);
                if (result.result == CConnAsyncTask.CQueryResult.SUCCESS && result.data != null) {
                    if (step != null) {
                        step.next(result.data);
                    }

                } else {
                    Logger.e(TAG, getString(R.string.text_network_data_rec_fail));
                    Logger.e(TAG, "CConnAsyncTask error=" + result.errorStr);
                    try {
                        CDialog.showDlg(getApplicationContext(), getString(R.string.text_network_data_rec_fail), new CDialog.DismissListener() {
                            @Override
                            public void onDissmiss() {
                                finish();
                            }
                        });
                    } catch (Exception e) {

                        try {
                            CDialog.showDlg(SplashActivity.this, getString(R.string.text_network_data_rec_fail), new CDialog.DismissListener() {
                                @Override
                                public void onDissmiss() {
                                    finish();
                                }
                            });
                        } catch (Exception e2) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                            builder.setMessage(getString(R.string.text_network_data_rec_fail));
                            builder.setPositiveButton(getString(R.string.text_confirm), null);
                            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                }
                            });
                            AlertDialog dlg = builder.create();
                            dlg.show();
                        }
                    }
                }
            }
        };

        CConnAsyncTask asyncTask = new CConnAsyncTask();
        asyncTask.execute(queryListener);
    }
}
