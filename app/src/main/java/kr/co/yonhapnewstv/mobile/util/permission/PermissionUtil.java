package kr.co.yonhapnewstv.mobile.util.permission;


import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

public class PermissionUtil {
    private static final String TAG = PermissionUtil.class.getSimpleName();

//    public static void requestPermissions(Activity activity, String[] permissions, int requestCode, IPermissionResult iPermissionResult) {
//        ArrayList<String> deniedPermissionList = new ArrayList<>();
//
//        for (String permission : permissions) {
//            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(activity, permission))
//                deniedPermissionList.add(permission);
//        }
//
//        if (!deniedPermissionList.isEmpty()) {
//            ActivityCompat.requestPermissions(activity, deniedPermissionList.toArray(new String[0]), requestCode);
//        } else {
//            iPermissionResult.onPermissionResult(requestCode, , permissions);
//        }
//    }

//    public static void requestPermission(BaseFragmentMedi fragment, String[] PERMISSIONS, IPermissionResult iPermissionResult) {
//        fragment.permissionRequest(PERMISSIONS, 0, iPermissionResult);
//    }


    /**
     * 권한 요청
     * 권한 처리 할 것 없을 경우 IPermissionResult.onPermissionResult()
     * @param fragment
     * @param permissions
     * @param requestCode
     * @param iPermissionResult
     */
    public static void requestPermissions(Fragment fragment, String[] permissions, int requestCode, IPermissionResult iPermissionResult) {
        if (fragment == null) {
            Log.e(TAG, "requestPermissions fragment is Null");
            return;
        }

        List<String> deniedPermissionList = PermissionUtil.getDeniedPermissionList(fragment.getActivity(), permissions);

        boolean isGranted = deniedPermissionList.isEmpty();
        if (isGranted == false) {
            fragment.requestPermissions(deniedPermissionList.toArray(new String[0]), requestCode);
        } else {
            if (iPermissionResult != null)
                iPermissionResult.onPermissionResult(requestCode, isGranted, deniedPermissionList);
        }
    }

    /**
     * 권한 요청
     * 권한 처리 할 것 없을 경우 IPermissionResult.onPermissionResult()
     * @param activity
     * @param permissions
     * @param requestCode
     * @param iPermissionResult
     */
    public static void requestPermissions(Activity activity, String[] permissions, int requestCode, IPermissionResult iPermissionResult) {
        if (activity == null) {
            Log.e(TAG, "requestPermissions Activity is Null");
            return;
        }

        List<String> deniedPermissionList = PermissionUtil.getDeniedPermissionList(activity, permissions);

        boolean isGranted = deniedPermissionList.isEmpty();
        if (isGranted == false) {
            ActivityCompat.requestPermissions(activity, permissions, requestCode);
        } else {
            if (iPermissionResult != null)
                iPermissionResult.onPermissionResult(requestCode, isGranted, deniedPermissionList);
        }
    }

    /**
     * 권한 결과 확인
     * 권한 처리 할 것 없을 경우 IPermissionResult.onPermissionResult()
     * @param context
     * @param requestCode
     * @param permissions
     * @param iPermissionResult
     */
    public static void permissionResultCheck(Context context, int requestCode, String[] permissions, IPermissionResult iPermissionResult) {
        List<String> deniedPermissionList = PermissionUtil.getDeniedPermissionList(context, permissions);
        boolean isGranted = deniedPermissionList.isEmpty();
        if (iPermissionResult != null)
            iPermissionResult.onPermissionResult(requestCode,  isGranted, deniedPermissionList);
    }

    /**
     * 권한 설정이 되었는지 여부
     * 하나라도 안 되어 있으면 false
     * @param permissions
     */
    public static boolean isGrantedPermissions(Context context, String... permissions) {
        if (context == null) {
            return false;
        }

        if(permissions == null) {
            return true;
        }
        for (String permission : permissions) {
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(context, permission))
                return false;
        }

        return true;
    }

    /**
     * 권한 설정이 안된 퍼미션 리스트를 리턴한다.
     * @param context
     * @param permissions
     * @return
     */
    public static List<String> getDeniedPermissionList(Context context, String... permissions) {
        List<String> persmissionList = new ArrayList<>();
        if (permissions == null) {
            Log.e(TAG, "요청된 퍼미션 없음");
            return persmissionList;
        }
        for (String permission : permissions) {
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(context, permission))
                persmissionList.add(permission);
        }

        return persmissionList;

    }

}
