package kr.co.yonhapnewstv.mobile.util;

public class TempDataUtil {

    private static TempDataUtil instance;

    private String str_PostCode = "";
    private String str_MainAddress = "";
    private String str_RemainAddress = "";
    private int int_buff = 0;

    private TempDataUtil(){ }

    public static TempDataUtil getInstance() {
        if (instance == null) {
            instance = new TempDataUtil();
        }
        return instance;
    }

    public void setAdressData(String PostCode, String MainAddress, String RemainAddress){
        str_PostCode = PostCode;
        str_MainAddress = MainAddress;
        str_RemainAddress = RemainAddress;
    }

    public void setData(int integer){
        int_buff = integer;
    }

    public String getPostCodeData(){
        return str_PostCode;
    }

    public String getMainAddressData(){
        return str_MainAddress;
    }

    public String getRemainAddressData(){
        return str_RemainAddress;
    }

    public int getIntData(){
        return int_buff;
    }
}
