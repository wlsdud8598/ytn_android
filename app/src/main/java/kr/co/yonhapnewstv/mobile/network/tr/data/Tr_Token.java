package kr.co.yonhapnewstv.mobile.network.tr.data;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.yonhapnewstv.mobile.network.tr.BaseData;
import kr.co.yonhapnewstv.mobile.network.tr.BaseUrl;

/**
 strJson={"pn":"y/n","dk":"token", "pk":"token","pf":"android"}

 */

public class Tr_Token extends BaseData {
	private final String TAG = Tr_Token.class.getSimpleName();


	public static class RequestData {
        public String pn;
        public String dk;
        public String pk;
        public String pf;
    }

	public Tr_Token() {
		super.conn_url = BaseUrl.COMMON_URL;
	}

	@Override
	public JSONObject makeJson(Object obj) throws JSONException {
		if (obj instanceof RequestData) {
            JSONObject body = new JSONObject();

            RequestData data = (RequestData) obj;

			body.put("pn", data.pn); //  push notification
			body.put("dk", data.dk); //  사용자 식별값
			body.put("pk", data.pk); //  push noti token
            body.put("pf", data.pf); //  platform

			return body;
		}

		return super.makeJson(obj);
	}

    /***************************************RECEIVE********************************************/

    @SerializedName("result") // "ok"
    public String result;
    @SerializedName("token") // token value
    public String token;


}
