package kr.co.yonhapnewstv.mobile.push;

import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.yonhapnewstv.mobile.component.PushUtils;

public class GCFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = GCFirebaseMessagingService.class.getSimpleName();

    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        PushUtils.acquireWakeLock(this);
        Log.i(TAG, "From: " + remoteMessage.getFrom());
        Log.i(TAG, "onMessageReceived.remoteMessage="+remoteMessage);
        if (remoteMessage.getData() == null) {
            Log.e(TAG, "************ FCM 메시지가 없습니다.");
            return;
        }


        String title2 = "";
        String body2 = "";
        String link2 = "";

//        if(remoteMessage.getNotification() != null){
//            title2 = remoteMessage.getNotification().getTitle();
//            body2 = remoteMessage.getNotification().getBody();
//
//            if(remoteMessage.getNotification().getLink() != null)
//                link2 = remoteMessage.getNotification().getLink().toString();
//
//            Log.d(TAG,"notification Title : "+ remoteMessage.getNotification().getTitle());
//            Log.d(TAG,"notification Body : "+ remoteMessage.getNotification().getBody());
//            Log.d(TAG,"notification Link: "+ remoteMessage.getNotification().getLink());
//
//        } else {
            String message = remoteMessage.getData().get("message");
//        String message = remoteMessage.getNotification().getBody();
            if (TextUtils.isEmpty(message)) {
                Log.e(TAG, "onMessageReceived::"+remoteMessage.getData());
                Log.e(TAG, "onMessageReceived.message::"+message);
                return;
            }

            //JSON parsing
            JSONObject json = null;
            try {
                json = new JSONObject(message);

                Log.i(TAG,"msg original : "+message);
                json.put("msgJSON", message);

                title2 = (String) json.get("title");
                body2 = (String) json.get("body");
                link2 = (String) json.get("link");
                Log.v(TAG, "title-" + title2 + " body-" + body2 + " link-" + link2);

            } catch (JSONException e) {
                e.printStackTrace();
            }
//        }






        NotiDummyActivity.showPushMessage(this, title2, body2, link2);


    }

}