package kr.co.yonhapnewstv.mobile.util.permission;

import java.util.List;

public interface IPermissionResult {
//    void onSuccess(int requestCode);
//    void onFail(int requestCode);

    void onPermissionResult(int requestCode, boolean isGranted, List<String> deniedPermissionList);
}
