package kr.co.yonhapnewstv.mobile.util;

import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.BounceInterpolator;

public class AnimUtil {

    public void bounceAnim(View view) {
        ObjectAnimator animY = ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, -40,0,-30,0,-20,0);
        animY.setDuration(3000);//1sec
        animY.setInterpolator(new BounceInterpolator());
//        animY.setRepeatCount(1);
        animY.start();
    }
}
