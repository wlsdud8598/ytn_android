package kr.co.yonhapnewstv.mobile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import kr.co.yonhapnewstv.mobile.component.CDialog;
import kr.co.yonhapnewstv.mobile.util.SharedPref;
import kr.co.yonhapnewstv.mobile.util.permission.IPermissionResult;
import kr.co.yonhapnewstv.mobile.util.permission.PermissionUtil;


public class MainActivity extends BaseActivity {
    public static MainActivity activity = null;


    private long backKeyPressedTime = 0;
    private Toast toast;

    private WebView mWebView;
    private ProgressBar spinner;
    private LinearLayout spinner_lv;

    private Intent intent;

    private String receivedToken;
    private String receivedUrl;

    private Boolean type = false;

    private final String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA};

    private OrientationEventListener oriEvent;
    private boolean canOriChange = true;
    private boolean canLandscape = false;
    private boolean is_main = false;
    private boolean is_finish = false;
    private boolean is_loading = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activity = this;

        mWebView = findViewById(R.id.main_webview);

        intent = getIntent();
        if (intent != null){
            receivedToken = getIntent().getStringExtra("encoded");
            receivedUrl = getIntent().getStringExtra("pushUrl");
        }

        settingWebview(mWebView);

        if (receivedUrl != null && !receivedUrl.equals("")) {
            Log.i(TAG, "Received push url: " + receivedUrl);
            mWebView.loadUrl(receivedUrl);
        }
        else {
            mWebView.loadUrl(Define.URL.BASE_URL);
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.createInstance(this);
        }

        spinner_lv = findViewById(R.id.progress_lv);

        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setMax(50);
        spinner.setProgress(1);

        oriEvent = new OrientationEventListener(this) {

            private int newOri = 0;

            private int oldOri = 0;

            @Override
            public void onOrientationChanged(int orientation) {
                if (Settings.System.getInt(MainActivity.this.getContentResolver(), "accelerometer_rotation", 0) != 0) {
                    if (orientation == -1)
                        return;
                    if (orientation >= 340 || orientation < 20) {
                        this.newOri = 1;
                    } else if (orientation >= 70 && orientation < 110) {
                        this.newOri = 2;
                    } else if (orientation >= 160 && orientation < 200) {
                        this.newOri = 1;
                    } else if (orientation >= 250 && orientation < 290) {
                        this.newOri = 2;
                    }
                    if (!MainActivity.this.canOriChange)
                        return;
                    if (this.oldOri == this.newOri)
                        return;
                    if (this.newOri == 2) {
                        is_finish = true;
                    } else if (this.newOri == 1) {
                        is_finish = false;
                        MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
                    }
                    this.oldOri = this.newOri;
                    return;
                }
            }
        };

        if (oriEvent.canDetectOrientation()) {
            oriEvent.enable();
        }

    }

    @SuppressLint("JavascriptInterface")
    private void settingWebview(WebView webView){
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webView.getSettings().setTextZoom(100);

        webView.setWebChromeClient(new ChromeClient());
        webView.setWebViewClient(new WebClient());
        webView.addJavascriptInterface(new AndroidBridge(), "Android");
        webView.addJavascriptInterface(new AndroidLandscapeBridge(), "landscape");

        // 웹뷰 디버깅 설정
        if(BuildConfig.DEBUG) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                WebView.setWebContentsDebuggingEnabled(BuildConfig.DEBUG);
            }
        }

        // 쿠키 허용 설정
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setAcceptThirdPartyCookies(mWebView, true);

            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.YEAR, 1); // 만료기간 1년 뒤
            String date = String.valueOf(calendar.getTime());

            String urlCookie = Define.URL.COOKIE_DOMAIN;
            String cookieData = "_ytvAppDeviceToken=" + receivedToken + "; Expires=" + date+ "; path=/; domain=" + urlCookie;

            cookieManager.setCookie(urlCookie, cookieData);
            Log.i(TAG, "COOKIE: "+ cookieManager.getCookie(urlCookie));

        }
    }


    class WebClient extends WebViewClient {
        public static final String INTENT_PROTOCOL_START = "intent:";
        public static final String INTENT_PROTOCOL_INTENT = "#Intent;";
        public static final String INTENT_PROTOCOL_END = ";end;";
        public static final String GOOGLE_PLAY_STORE_PREFIX = "market://details?id=";
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {


            String url = request.getUrl() == null ? "" : request.getUrl().toString();


            Log.d(TAG, "shouldOverrideUrlLoading.url="+url);

            if (url.startsWith("mailto:") || url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, request.getUrl());
                startActivity(intent);
                return true;
            } else if(url.startsWith("market://")){
                Intent intent = new Intent(Intent.ACTION_VIEW, request.getUrl());
                startActivity(intent);
                return true;
            } else if (url.startsWith("intent:kakaolink://")) {
                final int customUrlStartIndex = INTENT_PROTOCOL_START.length();
                final int customUrlEndIndex = url.indexOf(INTENT_PROTOCOL_INTENT);
                if (customUrlEndIndex < 0) {
                    return false;
                } else {
                    final String customUrl = url.substring(customUrlStartIndex, customUrlEndIndex);
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(customUrl)));
                    } catch (ActivityNotFoundException e) {
                        final int packageStartIndex = customUrlEndIndex + INTENT_PROTOCOL_INTENT.length();
                        final int packageEndIndex = url.indexOf(INTENT_PROTOCOL_END);

                        final String packageName = url.substring(packageStartIndex, packageEndIndex < 0 ? url.length() : packageEndIndex);
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_STORE_PREFIX + "com.kakao.talk")));
                    }
                    return true;
                }
            }  else {
                return super.shouldOverrideUrlLoading(view, request);
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d(TAG, "shouldOverrideUrlLoading.uri.getScheme()="+Uri.parse(url).getScheme());
            is_loading = true;

            Log.d(TAG, "shouldOverrideUrlLoading.url="+url);

            if (url.startsWith("mailto:") || url.startsWith("tel:")) {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                return true;
            } else if(url.startsWith("market://")){
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                return true;
            } else if (url.startsWith("intent:kakaolink://")) {
                final int customUrlStartIndex = INTENT_PROTOCOL_START.length();
                final int customUrlEndIndex = url.indexOf(INTENT_PROTOCOL_INTENT);
                if (customUrlEndIndex < 0) {
                    return false;
                } else {
                    final String customUrl = url.substring(customUrlStartIndex, customUrlEndIndex);
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(customUrl)));
                    } catch (ActivityNotFoundException e) {
                        final int packageStartIndex = customUrlEndIndex + INTENT_PROTOCOL_INTENT.length();
                        final int packageEndIndex = url.indexOf(INTENT_PROTOCOL_END);

                        final String packageName = url.substring(packageStartIndex, packageEndIndex < 0 ? url.length() : packageEndIndex);
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_STORE_PREFIX + "com.kakao.talk")));
                    }
                    return true;
                }
            } else {
                return super.shouldOverrideUrlLoading(view, url);
            }
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            spinner_lv.setVisibility(View.VISIBLE);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            spinner_lv.setVisibility(View.GONE);
            is_loading = false;

            if(BuildConfig.DEBUG){
                CDialog.showDlg(MainActivity.this,SharedPref.getInstance().getPreferences(SharedPref.TOKEN));
            }


            if(url.equals(Define.URL.BASE_URL) || url.equals(Define.URL.BASE_URL_SLASH)) {
                mWebView.loadUrl("javascript:setApplicationNotUpdate()");
                is_main = true;

            } else {
                is_main = false;
            }

            Log.d(TAG, "is_main : "+is_main);



            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                CookieSyncManager.getInstance().sync();
            } else {
                CookieManager.getInstance().flush();
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            spinner_lv.setVisibility(View.GONE);
        }
    }

    private View mCustomView;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;
    private int mOriginalOrientation;
    private int mOriginalSystemUiVisibility;


    class ChromeClient extends WebChromeClient {



        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            return super.onJsAlert(view, url, message, result);
        }

        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
//            return super.onShowFileChooser(webView, filePathCallback, fileChooserParams);
            Log.i(TAG, "onShowFileChooser");
            mFilePathCallback = filePathCallback;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                type = fileChooserParams.isCaptureEnabled();
            }
            Log.i(TAG, "type : " + type);
            reqPermissions();


            return true;
        }




        @Override
        public void onHideCustomView()
        {
            ((FrameLayout)getWindow().getDecorView()).removeView(mCustomView);
            mCustomView = null;
            getWindow().getDecorView().setSystemUiVisibility(mOriginalSystemUiVisibility);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
            mCustomViewCallback.onCustomViewHidden();
            mCustomViewCallback = null;
        }

        @Override
        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback)
        {
            if (mCustomView != null)
            {
                onHideCustomView();
                return;
            }

            mCustomView = paramView;
            mOriginalSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
            mOriginalOrientation = getRequestedOrientation();
            if(!is_finish)
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            else
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
            mCustomViewCallback = paramCustomViewCallback;

            ((FrameLayout)getWindow().getDecorView()).addView(mCustomView, new FrameLayout.LayoutParams(-1, -1));
            getWindow().getDecorView().setSystemUiVisibility(3846);

        }



    }


    /**
     * 권한 체크
     */
    private void reqPermissions() {
        PermissionUtil.permissionResultCheck(MainActivity.this, 999, PERMISSIONS, new IPermissionResult() {
            @Override
            public void onPermissionResult(int requestCode, boolean isGranted, List<String> deniedPermissionList) {
                if (isGranted) {
                    if(type){
                        takePhoto();
                    } else {
                        openFolder();
                    }

                } else {
                    permissionRequest(PERMISSIONS, 0, new IPermissionResult() {
                        @Override
                        public void onPermissionResult(int requestCode, boolean isGranted, List<String> deniedPermissionList) {
                            if (isGranted) {
                                if(type){
                                    takePhoto();
                                } else {
                                    openFolder();
                                }
                            } else {
                                reqPermissions();
                            }
                        }
                    });
                }
            }
        });
    }


    /**
     * 사진 파일로 사용할 파일 생성
     * @return
     */
    private File createPhotoFile() {
        File photoFile = null;
        String fileName = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // 임시로 사용할 파일의 경로를 생성
            if (getCacheDir().exists() == false) {
                getCacheDir().mkdirs();
            }
            photoFile = new File(getCacheDir(), fileName);
        } else {
            photoFile = new File(Environment.getExternalStorageDirectory(), fileName);
        }

        return photoFile;
    }

    private File mPhotoFile;
    protected Uri outputFileUri;
    private void takePhoto() {
        mPhotoFile = createPhotoFile();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            outputFileUri = FileProvider.getUriForFile(this, getPackageName(), mPhotoFile);
        } else {
            outputFileUri = Uri.fromFile(mPhotoFile);
        }
        Log.i(TAG, "takePhoto.outputFileUri="+outputFileUri);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(
                Intent.createChooser(intent, "사진촬영"),
                INPUT_FILE_REQUEST_CODE);
    }



    private static final int INPUT_FILE_REQUEST_CODE = 111;

    private ValueCallback<Uri> mUploadMessage;
    private ValueCallback<Uri[]> mFilePathCallback;
    private String mCameraPhotoPath;

    private void openFolder() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (intent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            photoFile = createPhotoFile();
            intent.putExtra("PhotoPath", mCameraPhotoPath);

            // Continue only if the File was successfully created
            if (photoFile != null) {
                mCameraPhotoPath = new File(photoFile.getPath()).getPath(); //"file:" + photoFile.getAbsolutePath();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this, getPackageName(), photoFile));
                } else {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                }

            } else {
                intent = null;
            }
        }

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "파일선택"),
                    INPUT_FILE_REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "사용 가능한 앱이 없습니다.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public class AndroidLandscapeBridge {
        @JavascriptInterface
        public void tabname(final String live){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(live.equals("live"))
                        canLandscape = true;
                    else
                        canLandscape = false;

                    Log.i(TAG, "tabname =" + live);
                }
            });
        }
    }

    public class AndroidBridge {
        @JavascriptInterface
        public void pushservice(final String pushNoti){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SharedPref.getInstance().savePreferences(SharedPref.PUSH_NOTI, pushNoti);

                    Log.i(TAG, "push notification =" + pushNoti);
                }
            });
        }

        @JavascriptInterface
        public void openLink(String url){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String url_decode = null;
                    try {
                        url_decode = URLDecoder.decode(url, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Uri uri = Uri.parse(url_decode);
                    Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                    List<ResolveInfo> resInfos = getPackageManager().queryIntentActivities(intent, 0);

                    Log.i("linksms","list: "+resInfos);
                    if(resInfos.size() <= 0) {
                        Log.e(TAG, "NOT FOUND ACTIVITY");
                        Toast.makeText(MainActivity.this,"열 수 있는 앱을 찾을 수 없습니다.",Toast.LENGTH_LONG).show();
                    } else {
                        startActivity(intent);
                    }
                    Log.i(TAG, "openLink =" + url);
                }
            });
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        overridePendingTransition(0, 0);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.getInstance().stopSync();
        }

        try{
            Class.forName("android.webkit.WebView")
                    .getMethod("onPause",(Class[]) null)
                    .invoke(mWebView, (Object[]) null);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.getInstance().startSync();
        }

        try{
            Class.forName("android.webkit.WebView")
                    .getMethod("onResume",(Class[]) null)
                    .invoke(mWebView, (Object[]) null);
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        if(mWebView.canGoBack()){
            mWebView.goBack();
        }else{
            backKey();
        }
    }

    public void backKey(){
        if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            showGuide();
            return;
        }
//        if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
//            this.finish();
//            toast.cancel();
//        }
    }

    public void showGuide() {
        CDialog.showDlg(this,"앱을 종료하시겠습니까?")
                .setOkButton("확인", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ActivityCompat.finishAffinity(MainActivity.this);
                        System.runFinalizersOnExit(true);
                        System.exit(0);
                    }
                })
                .setNoButton("취소", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
//        toast = Toast.makeText(this, "'뒤로'버튼 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT);
//        toast.show();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        receivedUrl = intent.getStringExtra("pushUrl");
        Log.i(TAG,"received Url : "+receivedUrl);

        if (receivedUrl != null && !receivedUrl.equals("")) {
            Log.i(TAG, "Received push url: " + receivedUrl);
            mWebView.loadUrl(receivedUrl);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            Uri[] results = null;
            if (requestCode == INPUT_FILE_REQUEST_CODE) {
                if (data != null) {
                    String dataString = data.getDataString();
                    if (dataString != null) {
                        results = new Uri[]{Uri.parse(dataString)};
                    }
                    if (mFilePathCallback != null)
                        mFilePathCallback.onReceiveValue(results);
                } else {
                    // If there is not data, then we may have taken a photo
                    if (mPhotoFile.getPath() != null) {
                        results = new Uri[]{outputFileUri};
                    } else {
                        results = null;
                    }
                    if (mFilePathCallback != null)
                        mFilePathCallback.onReceiveValue(results);
                }
                mFilePathCallback = null;
            }
        } else {
            if (requestCode == INPUT_FILE_REQUEST_CODE) {
                mFilePathCallback.onReceiveValue(null);
            }

            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void ExitFullScreen(){
        if(!is_loading) {
            if (is_main) {
                if (canLandscape) {
                    try {
                        new ChromeClient().onHideCustomView();
                        mWebView.loadUrl("javascript:(function(){if($('#live-player') != undefined){$('#live-player')[0].webkitExitFullscreen();}})();");
                    } catch (Exception e) {

                    }
                }
            } else {
                try {
                    new ChromeClient().onHideCustomView();
                    mWebView.loadUrl("javascript:(function(){if($('#live-player') != undefined){$('#live-player')[0].webkitExitFullscreen();}})();");
                } catch (Exception e) {

                }
            }
        }
    }

    public void FullScreen(){
        if(!is_loading) {
            if (is_main) {
                if (canLandscape) {
                    try {
                        mWebView.loadUrl("javascript:(function(){if($('#live-player') != undefined){$('#live-player')[0].webkitRequestFullscreen();}})();");
                    } catch (Exception e) {

                    }
                }
            } else {
                try {
                    mWebView.loadUrl("javascript:(function(){if($('#live-player') != undefined){$('#live-player')[0].webkitRequestFullscreen();}})();");
                } catch (Exception e) {

                }
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Configuration config = getResources().getConfiguration();
        if(config.orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            ExitFullScreen();
        } else {
            FullScreen();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState )
    {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        mWebView.restoreState(savedInstanceState);
    }
}
