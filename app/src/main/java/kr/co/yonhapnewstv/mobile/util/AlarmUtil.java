package kr.co.yonhapnewstv.mobile.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AlarmUtil {

    private static AlarmUtil instance;
    private Context mContext;

    private String mIndex;
    private String mParam1;
    private String mParam2;
    private String mTitle;
    private String mSubtitle;

    public static int ALARM_ID_LUCKY = 0;
    public static int ALARM_ID_COACH = 1;
    public static int ALARM_ID_TEST = 2;

    private AlarmUtil(Context context) {
        this.mContext=context;
    }

    public static AlarmUtil getInstance(Context context) {
        if (instance == null) {
            instance = new AlarmUtil(context);
        }
        return instance;
    }

    public AlarmUtil setData(String title, String subtitle, String index, String param1, String param2){
        Log.d("AlarmUtil", "AlarmDataSet - title : " + title + " / subtitle : " + subtitle + " / index : " + index + " / param1 : " + param1 + " / param2 : " + param2);
        this.mIndex = index;
        this.mParam1 = param1;
        this.mParam2 = param2;
        this.mTitle = title;
        this.mSubtitle = subtitle;

        return this;
    }

    public void setAlarm(int alarmId, int setHour, boolean isLoop) {
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(mContext, BroadCastUtil.class);
        intent.putExtra("push_index", mIndex);
        intent.putExtra("push_param1", mParam1);
        intent.putExtra("push_param2", mParam2);
        intent.putExtra("push_title", mTitle);
        intent.putExtra("push_subtitle", mSubtitle);

        Log.d("AlarmUtil", "AlarmIntentSet - title : " + mTitle + " / subtitle : " + mSubtitle + " / index : " + mIndex + " / param1 : " + mParam1 + " / param2 : " + mParam2);

        if(checkAlarm(alarmManager, intent, alarmId,0)) return;

        PendingIntent sender = PendingIntent.getBroadcast(mContext, alarmId, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int alramHour = setHour; // 알람을 시작할 시간
        if (alarmId == ALARM_ID_LUCKY && hour >= alramHour) {
            calendar.add(Calendar.DATE, 1);
        }
        if(alarmId == ALARM_ID_COACH){
            calendar.add(Calendar.DAY_OF_YEAR, 36);
        }
        calendar.set(Calendar.HOUR_OF_DAY, alramHour);
        calendar.set(Calendar.MINUTE, 0);

        if(isLoop)
//            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, sender);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, sender);
        else
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);

        Log.d("AlarmUtil", "Set Alarm!!!!!!!!!!!!!!!!!!!!!!!!"
                +new SimpleDateFormat("yyyy.MM.dd HH:mm").format(calendar.getTime()));

    }

    public void remove(int alarmId) {
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(mContext, BroadCastUtil.class);
        if(checkAlarm(alarmManager, intent, alarmId, -1)){
            Log.d("AlarmUtil", "Remove Alarm!!!!!!!!!!!!!!!!!!!!!!!!");
        }
    }

    public boolean checkAlarm(AlarmManager am, Intent intent, int alarmId, int mode){
        Log.d("AlarmUtil", "Check Alarm!!!!!!!!!!!!!!!!!!!!!!!!");
        PendingIntent sender = PendingIntent.getBroadcast(mContext, alarmId, intent, PendingIntent.FLAG_NO_CREATE);

        if(alarmId == ALARM_ID_TEST) return false;

        if(sender != null){
            //알람이 있는 경우 - 지울거나 그냥 두거나
            if(mode == -1){
                sender = PendingIntent.getBroadcast(mContext, alarmId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                am.cancel(sender);
                sender.cancel();
            }
            return true;
        }
        else {
            Log.d("AlarmUtil", "No Alarm!!!!!!!!!!!!!!!!!!!!!!!!");
            return false;
        }
    }
}
