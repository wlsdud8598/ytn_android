package kr.co.yonhapnewstv.mobile.util;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;

import java.util.Iterator;
import java.util.List;


/**
 * Created by User5 on 2015-05-20.
 */
public class Util {
    private static final String TAG = Util.class.getSimpleName();


    /**
     * 앱 실행중 여부 판단
     * @param context
     * @return
     */
    public static boolean getServiceTaskName(Context context, String activityName) {

        boolean checked = false;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> info = am.getRunningTasks(30);
        Log.i(TAG, "myTaskName() = " + context.getPackageName());


        for (int i = 0; i < info.size(); i++) {
            Log.i(TAG, "[" + i + "] getServiceTaskName:" + info.get(i).topActivity.getPackageName() + " >> " + info.get(i).topActivity.getClassName());
        }


        for (Iterator iterator = info.iterator(); iterator.hasNext();) {
            ActivityManager.RunningTaskInfo runningTaskInfo = (ActivityManager.RunningTaskInfo) iterator.next();
            Log.i(TAG, "getServiceTaskName().topActivity= "+runningTaskInfo.topActivity.getClassName());
            Log.i(TAG, "getServiceTaskName().baseActivity= "+runningTaskInfo.baseActivity.getClassName());
            Log.i(TAG, "getServiceTaskName().numRunning= "+runningTaskInfo.numRunning);



            if (runningTaskInfo.topActivity.getClassName().equals(activityName)) {
                Log.i(TAG, "getServiceTaskName() = true");
                return true;
            }

            if (runningTaskInfo.baseActivity.getClassName().equals(activityName)) {
                Log.i(TAG, "getServiceTaskName() = true");
                return true;
            }
        }
        return false;
    }
}

